<?php
namespace App\Serializer;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ProductNormalizer implements ContextAwareNormalizerInterface
{
    private $normalizer;

    private $requestStack;

    /**
     * ProductNormalizer constructor.
     *
     * @param ObjectNormalizer $normalizer
     * @param RequestStack     $requestStack
     */
    public function __construct(ObjectNormalizer $normalizer, RequestStack $requestStack)
    {
        $this->normalizer = $normalizer;
        $this->requestStack = $requestStack;
    }

    /**
     * @param mixed $product
     * @param null  $format
     * @param array $context
     *
     * @return array|bool|float|int|mixed|string
     */
    public function normalize($product, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($product, $format, $context);

        $request = $this->requestStack->getCurrentRequest();
        if (null !== $request && null !== $request->request->get('exchangeRate') && strtoupper($data['currency']) !== strtoupper($request->query->get('currency'))) {
            $data['price'] /= $request->request->get('exchangeRate');
        }

        if (isset($data['price'])) {
            $data['price'] = number_format($data['price'], 2);
        }

        return $data;
    }

    /**
     * @param $data
     * @param null  $format
     * @param array $context
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return $data instanceof Product;
    }
}
