<?php

namespace App\Controller;

use App\Service\ApiProductFactory;
use App\Service\ExchangeRatesApi;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiProductController
 *
 * @package App\Controller
 */
class ApiProductController extends ApiBaseRestController
{
    /**
     * @Post("/product", name="api_create_product")
     *
     * @param ApiProductFactory $apiProductFactory
     * @param Request           $request
     *
     * @return Response
     */
    public function createCategory(ApiProductFactory $apiProductFactory, Request $request): Response
    {
        try {
            $product = $apiProductFactory->create($request);
            $data = $this->setDataOkResponse('app.messages.success.default', 'product', $product);
            $code = Response::HTTP_OK;
        } catch (\Exception $ex) {
            $data = $this->setDataErrorResponse('app.messages.error.default', $ex->getMessage());
            $code = Response::HTTP_BAD_REQUEST;
        }

        $view = $this->setSerializationGroupToView(
            $this->view($data, $code),
            'get-product'
        );

        return $this->handleView($view);
    }

    /**
     * @Get("/product", name="api_index_product")
     *
     * @param ApiProductFactory $apiProductFactory
     *
     * @return Response
     */
    public function indexProducts(ApiProductFactory $apiProductFactory): Response
    {
        try {
            $products = $apiProductFactory->index();
            $data = $this->setDataOkResponse('app.messages.success.default', 'products', $products);
            $code = Response::HTTP_OK;
        } catch (\Exception $ex) {
            $data = $this->setDataErrorResponse('app.messages.error.default', $ex->getMessage());
            $code = Response::HTTP_BAD_REQUEST;
        }

        $view = $this->setSerializationGroupToView(
            $this->view($data, $code),
            'get-product'
        );

        return $this->handleView($view);
    }

    /**
     * @Get("/product/featured", name="api_index_product_featured")
     *
     * @param ApiProductFactory $apiProductFactory
     * @param ExchangeRatesApi  $exchangeRatesApi
     * @param Request           $request
     *
     * @return Response
     */
    public function indexProductsFeatured(ApiProductFactory $apiProductFactory, ExchangeRatesApi $exchangeRatesApi, Request $request): Response
    {
        try {
            if (null !== $request->query->get('currency')) {
                $exchangeRate = $exchangeRatesApi->getExchangeRate($request->query->get('currency'));
                $request->request->add(['exchangeRate' => $exchangeRate]);
            }
            $products = $apiProductFactory->indexFeatured();
            $data = $this->setDataOkResponse('app.messages.success.default', 'products', $products);
            $code = Response::HTTP_OK;
        } catch (\Exception $ex) {
            $data = $this->setDataErrorResponse('app.messages.error.default', $ex->getMessage());
            $code = Response::HTTP_BAD_REQUEST;
        }

        $view = $this->setSerializationGroupToView(
            $this->view($data, $code),
            'get-product-featured'
        );

        return $this->handleView($view);
    }
}
