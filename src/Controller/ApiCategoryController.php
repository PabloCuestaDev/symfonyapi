<?php

namespace App\Controller;

use App\Entity\Category;
use App\Service\ApiCategoryFactory;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiCategoryController
 *
 * @package App\Controller
 */
class ApiCategoryController extends ApiBaseRestController
{
    /**
     * @Post("/category", name="api_create_category")
     *
     * @param ApiCategoryFactory $apiCategoryFactory
     * @param Request            $request
     *
     * @return Response
     */
    public function createCategory(ApiCategoryFactory $apiCategoryFactory, Request $request): Response
    {
        try {
            $category = $apiCategoryFactory->create($request);
            $data = $this->setDataOkResponse('app.messages.success.default', 'category', $category);
            $code = Response::HTTP_OK;
        } catch (\Exception $ex) {
            $data = $this->setDataErrorResponse('app.messages.error.default', $ex->getMessage());
            $code = Response::HTTP_BAD_REQUEST;
        }

        $view = $this->setSerializationGroupToView(
            $this->view($data, $code),
            'get-category'
        );

        return $this->handleView($view);
    }

    /**
     * @Put("/category/{category}", name="api_update_category")
     *
     * @param ApiCategoryFactory $apiCategoryFactory
     * @param Request            $request
     * @param Category           $category
     *
     * @return Response
     */
    public function updateCategory(ApiCategoryFactory $apiCategoryFactory, Request $request, Category $category): Response
    {
        try {
            $category = $apiCategoryFactory->update($request, $category);
            $data = $this->setDataOkResponse('app.messages.success.default', 'category', $category);
            $code = Response::HTTP_OK;
        } catch (\Exception $ex) {
            $data = $this->setDataErrorResponse('app.messages.error.default', $ex->getMessage());
            $code = Response::HTTP_BAD_REQUEST;
        }

        $view = $this->setSerializationGroupToView(
            $this->view($data, $code),
            'get-category'
        );

        return $this->handleView($view);
    }

    /**
     * @Delete("/category/{category}", name="api_delete_category")
     *
     * @param ApiCategoryFactory $apiCategoryFactory
     * @param Category           $category
     *
     * @return Response
     */
    public function deleteCategory(ApiCategoryFactory $apiCategoryFactory, Category $category): Response
    {
        try {
            $apiCategoryFactory->delete($category);
            $data = $this->setDataOkResponse('app.messages.success.default', 'category', []);
            $code = Response::HTTP_OK;
        } catch (\Exception $ex) {
            $data = $this->setDataErrorResponse('app.messages.error.default', $ex->getMessage());
            $code = Response::HTTP_BAD_REQUEST;
        }

        $view = $this->setSerializationGroupToView(
            $this->view($data, $code),
            'get-category'
        );

        return $this->handleView($view);
    }
}
