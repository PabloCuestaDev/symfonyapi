<?php

namespace App\Controller;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ApiBaseRestController
 *
 * @package App\Controller
 */
class ApiBaseRestController extends AbstractFOSRestController
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * ApiBaseRestController constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Set Serialization Group to view.
     *
     * @param View   $view
     * @param string $group
     *
     * @return View
     */
    public function setSerializationGroupToView(View $view, string $group): View
    {
        $context = new Context();
        $context->addGroup($group);
        $view->setContext($context);

        return $view;
    }

    /**
     * @param string $message
     * @param string $waitedKey
     * @param mixed  $object
     *
     * @return array
     */
    public function setDataOkResponse(string $message, string $waitedKey, $object): array
    {
        $data = [];

        $data['message'] = $this->translator->trans($message);
        $data[$waitedKey] = $object;

        return $data;
    }

    /**
     * @param string $errorMessage
     * @param mixed  $exceptionMessage
     *
     * @return array
     */
    public function setDataErrorResponse(string $errorMessage, $exceptionMessage): array
    {
        $data = [];

        $data['message'] = $this->translator->trans($errorMessage);
        $data['exception'] = $exceptionMessage;
        $message = @unserialize($data['exception'], ['allowed_classes' => false]);
        if (false !== $message && \is_array($message)) {
            $data['exception'] = $message;
            $data['error'] = $message;
        }

        return $data;
    }
}
