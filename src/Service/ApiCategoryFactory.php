<?php

namespace App\Service;

use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ApiCategoryFactory.
 */
class ApiCategoryFactory
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * ApiActivityFactory constructor.
     *
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface   $formFactory
     * @param ValidatorInterface     $validator
     */
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     *
     * @return Category
     *
     * @throws \Exception
     *
     */
    public function create(Request $request): Category
    {
        $category = new Category();
        $form = $this->formFactory->create(CategoryType::class, $category, ['method' => 'POST', 'validation_groups' => 'api_create']);
        $form->submit($request->request->all());
        $errors = $this->validator->validate($category);
        if ($form->isValid() && 0 === \count($errors)) {
            $this->em->persist($category);
            $this->em->flush();
        } else {
            $errorMessages = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            foreach ($form->getErrors(true) as $error) {
                $errorMessages[] = $error->getMessage();
            }

            throw new \RuntimeException(implode('. ', $errorMessages));
        }

        return $category;
    }

    /**
     * @param Request  $request
     * @param Category $category
     *
     * @return Category
     *
     * @throws \Exception
     *
     */
    public function update(Request $request, Category $category): Category
    {
        $form = $this->formFactory->create(CategoryType::class, $category, ['method' => 'POST', 'validation_groups' => 'api_create']);
        $form->submit($request->request->all());
        $errors = $this->validator->validate($category);
        if ($form->isValid() && 0 === \count($errors)) {
            $this->em->persist($category);
            $this->em->flush();
        } else {
            $errorMessages = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            foreach ($form->getErrors(true) as $error) {
                $errorMessages[] = $error->getMessage();
            }

            throw new \RuntimeException(implode('. ', $errorMessages));
        }

        return $category;
    }

    /**
     * @param Category $category
     */
    public function delete(Category $category): void
    {
        $this->em->remove($category);
        $this->em->flush();
    }
}