<?php

namespace App\Service;
use App\Entity\Product;

/**
 * Class ExchangeRatesApi
 *
 * @package App\Service
 */
class ExchangeRatesApi
{
    private $apiURL = 'https://api.exchangeratesapi.io/latest?base=%s&symbols=%s';

    private $baseCurrency = Product::DOLLAR_CURRENCY;

    private $symbols = Product::EURO_CURRENCY;

    /**
     * @param string $currency
     *
     * @return mixed
     *
     * @throws \ErrorException
     */
    public function getExchangeRate(string $currency)
    {
        if (!$this->isValidCurrency($currency)) {
            throw new \ErrorException();
        }
        $this->setBaseCurrency($currency);
        $symbols = (Product::DOLLAR_CURRENCY === $currency) ? Product::EURO_CURRENCY : Product::DOLLAR_CURRENCY;
        $this->setSymbolsCurrency($symbols);
        $url = sprintf($this->apiURL, $this->baseCurrency, $this->symbols);
        $data = json_decode(file_get_contents($url), true);

        return $data['rates'][$this->symbols];
    }

    /**
     * @param string $currency
     */
    public function setBaseCurrency(string $currency): void
    {
        $this->baseCurrency = $currency;
    }

    /**
     * @param string $symbols
     */
    public function setSymbolsCurrency(string $symbols): void
    {
        $this->symbols = $symbols;
    }

    /**
     * @param string $currency
     *
     * @return bool
     */
    private function isValidCurrency(string $currency): bool
    {
        return in_array($currency, Product::CURRENCIES, true);
    }
}