<?php

namespace App\Service;

use App\Entity\Product;
use App\Form\ProductType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ApiProductFactory.
 */
class ApiProductFactory
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * ApiActivityFactory constructor.
     *
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface   $formFactory
     * @param ValidatorInterface     $validator
     */
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     *
     * @return Product
     *
     * @throws \Exception
     *
     */
    public function create(Request $request): Product
    {
        $product = new Product();
        $data = $this->cleanRequest($request->request->all());
        $form = $this->formFactory->create(ProductType::class, $product, ['method' => 'POST', 'validation_groups' => 'api_create']);
        $form->submit($data);
        $errors = $this->validator->validate($product);
        if ($form->isValid() && 0 === \count($errors)) {
            $this->em->persist($product);
            $this->em->flush();
        } else {
            $errorMessages = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            foreach ($form->getErrors(true) as $error) {
                $errorMessages[] = $error->getMessage();
            }

            throw new \RuntimeException(implode('. ', $errorMessages));
        }

        return $product;
    }

    /**
     * @return object[]
     */
    public function index(): array
    {
        return $this->em->getRepository('App:Product')->findAll();
    }

    /**
     * @return object[]
     */
    public function indexFeatured(): array
    {
        return $this->em->getRepository('App:Product')->findBy(['featured' => true]);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function cleanRequest(array $data): array
    {
        if (isset($data['featured'])
            && ('false' === $data['featured'] || 'null' === $data['featured'] || !$data['featured']))
        {
            $data['featured'] = false;
        }

        return $data;
    }
}