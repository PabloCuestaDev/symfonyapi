<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @UniqueEntity("name")
 */
class Product
{
    public const EURO_CURRENCY = 'EUR';

    public const DOLLAR_CURRENCY = 'USD';

    public const CURRENCIES = [self::EURO_CURRENCY, self::DOLLAR_CURRENCY];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"get-product", "get-product-featured"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Groups({"get-product", "get-product-featured"})
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Groups({"get-product", "get-product-featured"})
     *
     * @Assert\Valid
     */
    protected $category;

    /**
     * @ORM\Column(type="float")
     *
     * @Groups({"get-product", "get-product-featured"})
     */
    protected $price;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"get-product", "get-product-featured"})
     *
     * @Assert\Choice(choices=Product::CURRENCIES, message="product.currency.not_valid")
     */
    protected $currency;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups({"get-product"})
     */
    protected $featured;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Product
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     *
     * @return Product
     */
    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return Product
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return Product
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getFeatured(): ?bool
    {
        return $this->featured;
    }

    /**
     * @param bool|null $featured
     * 
     * @return Product
     */
    public function setFeatured(?bool $featured): self
    {
        $this->featured = $featured;

        return $this;
    }
}
